import java.util.Scanner;
public class GameLauncher{
	public static void main(String[]args){
		Scanner reader=new Scanner(System.in);
		//Part 1:Greeting
		System.out.println("Hi, Welcome to the World Game Launcher!");
		
		//Part 2: Choosing game
		Boolean isProgramOpen=true;
		while(isProgramOpen){
			System.out.println("Please type one number to continue.\n 1.Hangman \n 2.Wordle \n 3.Quit");
			int playerchoose=reader.nextInt();
			if(playerchoose==1){
				//Player want playe Hangman
				Hangman.startHangmanGame();
			}else if(playerchoose==2){
				//Player want to play Wordle
				Wordle.startWordleGame();
			}else if(playerchoose==3){
				//Player quit 
				isProgramOpen=false;
			}
		}
		
		
	}
}